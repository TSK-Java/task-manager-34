package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.util.DateUtil;

public abstract class AbstractProjectShowCommand extends AbstractProjectCommand {

    protected void showProject(@Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("Имя: " + project.getName());
        System.out.println("Описание: " + project.getDescription());
        System.out.println("Статус: " + Status.toName(project.getStatus()));
        System.out.println("Дата создания: " + DateUtil.toString(project.getCreated()));
        System.out.println("Дата начала: " + DateUtil.toString(project.getDateBegin()));
    }

}
